﻿using UnityEngine;
using System.Collections;

public class Vial {

    // Vial stats, stats are 0 to 100
    public int fire = 0;
    public int water = 0;
    public int grass = 0;
    public float size = 1;

    public Color color
    {
        get
        {
            // Generate a color that represents our vial.
            Color c = new Color();

            c.r = (float)fire / 100.0f;
            c.b = (float)water / 100.0f;
            c.g = (float)grass / 100.0f;

            c.a = 1.0f; // Full alpha

            return c;
        }
    }

    public float damage
    {
        get
        {
            return (fire + water + grass) * size;
        }
    }

    public void Randomize()
    {
        // Randomize the stats of this vial.
        fire = Random.Range(0, 100);
        water = Random.Range(0, 100);
        grass = Random.Range(0, 100);

        size = Random.Range(0.1f, 2.0f);
    }
}
