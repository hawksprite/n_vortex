﻿using UnityEngine;
using System.Collections;
using System;

public class SpellProjectile : MonoBehaviour {

    [HideInInspector]
    public GameObject deathPrefab;

    private Vector3 direction;
    private float velocity;
    private float damage;
    private Vial p;
    private bool setup = false;
    private DateTime birth;

    // Visual stuff
    private SpriteRenderer r;
    private Rigidbody2D rigid;

	// Use this for initialization
	void Start () {
        birth = DateTime.Now;
        rigid = GetComponent<Rigidbody2D>();
	}

    private bool applyForce = false;
	// Update is called once per frame
	void Update () {
        if (setup)
        {
            // Calculate our modulus
            Vector3 change = direction * velocity * Time.smoothDeltaTime;
            // Make sure z is 0'ed out.
            Vector3 newSet = transform.position + change; newSet.z = 0;
            // Apply the velocity of the projectile.
            transform.position = newSet;

            /*
            if (applyForce)
            {
                applyForce = false;
                Vector3 change = direction * velocity * Time.smoothDeltaTime;
                Vector2 force = new Vector2(change.x, change.y);
                rigid.AddForce(force);
            }*/
        }

        if ((DateTime.Now - birth).TotalSeconds >= 5)
        {
            Destroy(this.gameObject); // If we life more than 5 seconds just play it safe and kill ourselves.
        }
	}

    public void Setup(Vector3 direction, float velocity, GameObject death, Vial parent)
    {
        deathPrefab = death;
        p = parent;

        this.direction = direction;
        this.velocity = velocity;
        this.damage = parent.damage;

        r = GetComponent<SpriteRenderer>();

        // Use our vial to kind of tweak our projectile
        transform.localScale = new Vector3(parent.size, parent.size, 1.0f);

        r.color = parent.color;

        setup = true;
        applyForce = true; // Flag to apply force to our rigid body
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Player")
        {
            Death();
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag != "Player")
        {
            Death();
        }
    }

    void Death()
    {
        // Hit something other then the player, hanlde it accordingly.
        if (deathPrefab)
        {
            GameObject.Instantiate(deathPrefab, transform.position, transform.rotation);
        }

        Destroy(this.gameObject); // Destroy our selves
    }
}
