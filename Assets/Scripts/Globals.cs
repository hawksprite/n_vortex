﻿using UnityEngine;
using System.Collections;

public static class Globals {
    public static bool gameLoaded = false;
    public static bool mapFinished = false;

    public static int tilesLoadedPerFrame = 50;
}
