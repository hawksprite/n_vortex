﻿using UnityEngine;
using System.Collections;

public class BackdropManager : MonoBehaviour {

    Transform c;

	// Use this for initialization
	void Start () {
        c = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(c.position.x, c.position.y, 0);
	}
}
