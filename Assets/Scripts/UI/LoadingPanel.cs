﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingPanel : MonoBehaviour {

    private Color origin;
    private bool doneLoading = false;
    public float depthFadeSpeed = 10.0f;

    public Text progressText;

	// Use this for initialization
	void Start () {
        origin = Camera.main.backgroundColor;
        Camera.main.backgroundColor = Color.black;
	}

    bool firstUpdate = true;
    bool secondUpdate = true;
    // For linking with the tiled reader
    bool hasReader = false;
    private TiledReader reader;

	// Update is called once per frame
	void Update () {

        if (firstUpdate)
        {
            firstUpdate = false;
            
            //Library.instance.loadLibrary(); // Call for the library to start it's loading thread.
            StartCoroutine("loadGame");
        }

        if (Library.instance.loadThreadComplete)
        {
            if (secondUpdate)
            {
                secondUpdate = false;

                // Now start our async scene load.
                //
                
            }
        }

        if (hasReader)
        {
            // Safe to start updating our progress display
            int left = reader.maxQueSize - reader.conQue.Count;
            progressText.text = left.ToString() + " / " + reader.maxQueSize.ToString();
            double percentage = (double)left / (double)reader.maxQueSize;

            // Fade from black back to what it originally was.
            Camera.main.backgroundColor = Color.Lerp(Color.black, origin, (float)(percentage));
        }

        if (doneLoading)
        {
            // Only close off the loading panel when our map is finished up.
            if (Globals.mapFinished)
            {
                Camera.main.backgroundColor = origin;

                // Kill this panel
                Destroy(this.gameObject);
            }
        }
        else
        {
            
        }
	}

    public void recieveReader(TiledReader t)
    {
        hasReader = true;
        reader = t;
    }

    IEnumerator loadGame()
    {
        AsyncOperation async = Application.LoadLevelAdditiveAsync("load");
        yield return async;
        doneLoading = true; // Flag that we're done loading
        Globals.gameLoaded = true; // Flag the global game loaded
    }
}
