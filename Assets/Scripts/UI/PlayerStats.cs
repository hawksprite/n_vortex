﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerStats : MonoBehaviour {

    private Text t;
    private bool first = true;
    private CharacterControllerSpell cSpell;
    private GameObject player;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void Setup(GameObject p)
    {
        t = GetComponent<Text>();

        player = p;
        cSpell = player.GetComponent<CharacterControllerSpell>();

        // Build our debug text
        t.text = "Current Vial:\n";
        t.text += "fire: " + cSpell.activeVial.fire.ToString() + "\n";
        t.text += "water: " + cSpell.activeVial.water.ToString() + "\n";
        t.text += "grass: " + cSpell.activeVial.grass.ToString() + "\n";
        t.text += "scale: " + cSpell.activeVial.size.ToString();

        // Set the color to visible again
        Color c = t.color;
        c.a = 1.0f;
        t.color = c;
    }
}
