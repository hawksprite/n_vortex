﻿using UnityEngine;
using System.Collections;

public class TileConstructor : MonoBehaviour {

    public int type = 0;

    SpriteRenderer sprite;
    TiledReader p;
    PolygonCollider2D c;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Construct(TiledReader parent, bool collision, int type, string layer)
    {
        if (type == 0)
        {
            //this.gameObject.SetActive(false); // If it's a type 0 then this is a dead entry
            DestroyImmediate(this.gameObject);
        }
        else
        {
            sprite = GetComponent<SpriteRenderer>();

            this.type = type;
            p = parent;
            sprite.sprite = Library.instance.getTypeSprite(this.type);

            // See if it's an edge panel
            if (layer == "Edge")
            {
                // Remove the polygon collider, it's hard set for the player issue
                DestroyImmediate(gameObject.GetComponent<PolygonCollider2D>());

                EdgeCollider2D e = (EdgeCollider2D)gameObject.AddComponent<EdgeCollider2D>();
                e.offset = new Vector2(0, 0.64f);
                //e.isTrigger = true;

                this.gameObject.layer = 10; // Set the collision layer to 10
            }
            else
            {
                if (layer == "Decoration")
                {
                    // Don't add a collider to a decoration
                }
                else
                {
                    // Otherwise check for normal collision types.
                    if (collision)
                    {
                        // If we're going to be collideing then go ahead and now dynamically apply a polygon collider
                        c = (PolygonCollider2D)gameObject.AddComponent<PolygonCollider2D>();
                    }
                }
            }
        }
    }

    public bool isCornerPiece(int type)
    {
        if (type == 25 || type == 76 || type == 101)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
