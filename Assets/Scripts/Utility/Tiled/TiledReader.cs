﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System;


[ExecuteInEditMode]
public class TiledReader : MonoBehaviour {

    public string mapJsonName = "";

    public bool buildMapStaged = true;

    public Vector3 rootMapLocation = Vector3.zero;

    public Vector2 tileMargin = Vector2.zero;

    public int maxQueSize = 0;

    [HideInInspector]
    public JSONNode parent;

    [HideInInspector]
    public float locationSpacePercentage = 1.0f;
    [HideInInspector]
    public int mWidth = 0;
    [HideInInspector]
    public int mHeight = 0;

    [HideInInspector]
    public GameObject tileConstructionPrefab;

    [HideInInspector]
    public GameObject playerPrefab;

    [HideInInspector]
    public bool finishedLoading = false;

    [HideInInspector]
    public List<TileReaderShell> conQue = new List<TileReaderShell>();

	void Start () {
        // Find the construction prefab
        //
        //playerPrefab = Resources.Load("Prefabs/Player", typeof(GameObject)) as GameObject;
        finishedLoading = true;
        Globals.mapFinished = true;
	}

    [ContextMenu("Process")]
    public void Begin()
    {
        tileConstructionPrefab = Resources.Load("Prefabs/TileConstructionPrefabDynamic", typeof(GameObject)) as GameObject;
        // Start the map building process
        StartCoroutine("LTiledMap");
    }

    [ContextMenu("Clean")]
    public void CleanDanger()
    {
        TileConstructor[] l = GameObject.FindObjectsOfType<TileConstructor>();

        for (int i = 0; i < l.Length; i++)
        {
            DestroyImmediate(l[i].gameObject);
        }
    }

    bool firstUpdate = true;
    void Update()
    {
        if (firstUpdate)
        {
            firstUpdate = false;

            
        }

        if (buildMapStaged)
        {
            // We need to manage our build que
            if (conQue.Count == 0)
            {
                // We're done!
                buildMapStaged = false;
                finalizeLoad();
            }
            else
            {
                // Otherwise construct some items out of our que.
                for (int i = 0; i < Globals.tilesLoadedPerFrame; i++)
                {
                    // Try to consturct the top object in the que
                    if (conQue.Count > 0)
                    {
                        // Construct the top tile
                        ConstructTile(conQue[0].collision, conQue[0].i, conQue[0].type, conQue[0].layer);
                        // Drop it from the que
                        conQue.RemoveAt(0);
                    }
                }
            }
        }
    }

    public IEnumerator LTiledMap()
    {
        // Read the json from the given name
        string location = "maps/" + mapJsonName;
        Debug.Log("Reading json from " + location);
        TextAsset jsonReader = (TextAsset)Resources.Load(location, typeof(TextAsset));
        string raw = jsonReader.text;

        parent = JSON.Parse(raw);

        // Pull the map configuration
        mWidth = parent["width"].AsInt;
        mHeight = parent["height"].AsInt;

        // Read the sprite sheet name into the library
        Library.instance.spriteSheetName = parent["tilesets"][0]["name"].Value;

        // NOTE: This is... well I mean.... it shouldn't fail?  But my god this is bad practice.
        // *** DEPRECEATED ***
        locationSpacePercentage = (float)parent["tilewidth"].AsInt;


        for (int i = 0; i < parent["layers"].Count; i++)
        {
            string name = parent["layers"][i]["name"];
            JSONArray data = parent["layers"][i]["data"].AsArray;
            bool shouldCollide = true;

            if (name == "Decoration")
            {
                shouldCollide = false;
            }

            // For each tile in data, submit it for construction
            for (int t = 0; t < data.Count; t++)
            {
                if (buildMapStaged) {
                    // We're staging the build, so just add it to our que for now.
                    conQue.Add(new TileReaderShell() { collision = shouldCollide, i = t, type = data[t].AsInt, layer = name });
                }
                else {
                    ConstructTile(shouldCollide, t, data[t].AsInt, name);
                }
                
            }
        }

        if (!buildMapStaged)
        {
            finalizeLoad(); // We can finialize our load now.
        }
        else
        {
            maxQueSize = conQue.Count;
            GameObject.FindObjectOfType<LoadingPanel>().recieveReader(this); // Tell the loading panel it can start showing progress.
        }

        // Finish off the coroutine.
        yield return null;
    }


    public void finalizeLoad()
    {
        // We should be done constructing the map.
        // Spawn the player
        //GameObject g = (GameObject)GameObject.Instantiate(playerPrefab, new Vector3(62, -115, 0), Quaternion.identity);
        //Camera.main.GetComponent<SmoothFollow>().recievePlayer(g.transform); // Report the new player object to our camera so it can prepare it's follow script.

        // Done loading
        finishedLoading = true;


        
    }

    public void ConstructTile(bool collision, int i, int type, string layer)
    {
        // Determine our grid location
        int x = i % mWidth;
        int y = i / mHeight;

        x *= -1; // Inverse the X axis placement

        // Add it to the root map location, then get the percentage of the grid location we should use as the actual location.
        Vector3 loc = rootMapLocation - new Vector3((float)x, (float)y, 0);
        
        // Apply the tile margin
        loc.x += tileMargin.x * x * -1; // Invert to corispond with the next axis invert to line it up correctly.
        loc.y += tileMargin.y * y * -1;

        GameObject t = GameObject.Instantiate(tileConstructionPrefab, loc, Quaternion.identity) as GameObject;
        t.transform.parent = this.transform; // Set the tile to be childed to this object.
        t.GetComponent<TileConstructor>().Construct(this, collision, type, layer); // Call the constructor script within our new tile.
    }
}

public class TileReaderShell
{
    public bool collision;
    public int i;
    public int type;
    public string layer;
}