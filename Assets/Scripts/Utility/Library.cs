﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class Library : MonoBehaviour {

    public int spriteIDOffset = 1;

    [HideInInspector]
    public string spriteSheetName = "spritesheet_complete";

    public bool loadThreadComplete = false;

    public bool loadLibraryLive = true;

    private static Library _instance;

    public static Library instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Library>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    // Simple little hashmap esc utility for handling the sprite library.
    // NOTE: spriteType = 0 will never actually be a tile, so set the first object as your "fallback" tile.
    // Public sets for the library
    public List<Sprite> spriteLibrary = new List<Sprite>();
    public List<int> spriteType = new List<int>();

    public Sprite getTypeSprite(int t)
    {
        loadLibrary();

        for (int i = 0; i < spriteType.Count; i++)
        {
            if (spriteType[i] == t)
            {
                return spriteLibrary[i];
            }
        }

        // Fallback to our only entry
        return spriteLibrary[0];
    }

    private bool hasLoaded = false;
    public void loadLibrary()
    {
        if (!hasLoaded)
        {
            // Only let this run once
            hasLoaded = true;

            //Thread loaderThread = new Thread(new ThreadStart(libraryThread));
            //loaderThread.Start();
            //libraryThread();
        }
    }

    private void libraryThread()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("Assets/Tileset/" + spriteSheetName);

        for (int i = 0; i < sprites.Length; i++)
        {
            // Another rogue/sloppy method for this work around.   But heck, it works.
            string sName = sprites[i].name;
            sName = sName.Replace(spriteSheetName + "_", "");

            int sId = int.Parse(sName);

            sId += spriteIDOffset;

            if (spriteType.Contains(sId) == false)
            {
                // This if statments allows us to pre-load fixes for tiles that aren't labeled properlly.
                spriteLibrary.Add(sprites[i]);
                spriteType.Add(sId); // Apply our loop location, and the given ID offset.
            }
        }

        loadThreadComplete = true;

    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }
}
