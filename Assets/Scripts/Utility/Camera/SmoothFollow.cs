﻿using UnityEngine;
using System.Collections;



public class SmoothFollow : MonoBehaviour
{
    [HideInInspector]
    public Transform target;
    public float smoothDampTime = 0.2f;
    [HideInInspector]
    public new Transform transform;
    public Vector3 cameraOffset;
    public bool useFixedUpdate = false;

    private CharacterController2D _playerController;
    private Vector3 _smoothDampVelocity;

    private Camera cam;

    private float lockedZ = 0;
    public bool lockZ = true;

    public float scrollSpeed = 20.0f;

    private bool ready = false;


    void LateUpdate()
    {
        if (ready)
        {
            if (!useFixedUpdate)
                updateCameraPosition();
        }
    }


    void FixedUpdate()
    {
        if (ready)
        {
            if (useFixedUpdate)
                updateCameraPosition();

            // Scroll the size of the camera with the mouse wheel
            cam.orthographicSize += Input.GetAxis("Mouse ScrollWheel") * scrollSpeed; // Don't need delta time since this is a fixed update
        }
    }

    public void recievePlayer(Transform p)
    {
        target = p;

        transform = gameObject.transform;
        _playerController = target.GetComponent<CharacterController2D>();
        cam = GetComponent<Camera>();

        lockedZ = transform.position.z;

        ready = true;
    }


    void updateCameraPosition()
    {
        if (_playerController == null)
        {
            Vector3 temp = Vector3.SmoothDamp(transform.position, target.position - cameraOffset, ref _smoothDampVelocity, smoothDampTime);

            if (lockZ)
            {
                temp.z = lockedZ;
            }

            transform.position = temp;
            return;
        }

        if (_playerController.velocity.x > 0)
        {
            Vector3 temp = Vector3.SmoothDamp(transform.position, target.position - cameraOffset, ref _smoothDampVelocity, smoothDampTime);

            if (lockZ)
            {
                temp.z = lockedZ;
            }

            transform.position = temp;
        }
        else
        {
            var leftOffset = cameraOffset;
            leftOffset.x *= -1;

            Vector3 temp = Vector3.SmoothDamp(transform.position, target.position - leftOffset, ref _smoothDampVelocity, smoothDampTime);

            if (lockZ)
            {
                temp.z = lockedZ;
            }


            transform.position = temp;
        }
    }

}
