﻿using UnityEngine;
using System.Collections;
using System;

public class SpriteAnimation : MonoBehaviour {

    public Sprite[] spriteSet;
    public float animationDelay = 200;
    public bool destroyOnDone = true;
    private int currentFrame = 0;

    private SpriteRenderer rend;

    DateTime lastFrame;

	// Use this for initialization
	void Start () {
        lastFrame = DateTime.Now;
        rend = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if ((DateTime.Now - lastFrame).TotalMilliseconds >= animationDelay)
        {
            currentFrame++;

            if (currentFrame >= spriteSet.Length)
            {
                if (destroyOnDone)
                {
                    DestroyImmediate(this.gameObject);
                }
                else
                {
                    currentFrame = 0;
                }
            }
            else
            {
                rend.sprite = spriteSet[currentFrame];
            }

            lastFrame = DateTime.Now;
        }
	}
}
