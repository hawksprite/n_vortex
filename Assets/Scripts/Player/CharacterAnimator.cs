﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CharacterAnimator : MonoBehaviour {

    private int activeAnimation = 0;
    private int activeFrame = 0;
    private SpriteRenderer rend;

    public float animationSpeed { get { return animationSpeeds[activeAnimation]; } }

    public List<Sprite[]> animationLibrary = new List<Sprite[]>();
    public List<float> animationSpeeds = new List<float>();

	// Use this for initialization
	void Start () {
        // Hard code loading the frames for the animations
        helperLoadAnimation("Idle", 10, 100);
        helperLoadAnimation("Run", 8, 50);
        helperLoadAnimation("Jump", 10, 200);
        helperLoadAnimation("Shoot", 3, 75);
        helperLoadAnimation("Slide", 5, 75);

        lastFrame = DateTime.Now;
        rend = GetComponent<SpriteRenderer>();
	}

    private void helperLoadAnimation(string name, int max, float speed)
    {
        Sprite[] buff = new Sprite[max];

        for (int i = 0; i < max; i++)
        {
            int file = i + 1;
            buff[i] = (Sprite)Resources.Load("Assets/Player/Adventure Girl/" + name + " (" + file.ToString() + ")", typeof(Sprite));
        }

        animationLibrary.Add(buff);
        animationSpeeds.Add(speed);
    }
	
    // Time stuff for the animation
    private DateTime lastFrame;
    
	// Update is called once per frame
	void Update () {
	    // Update the animation frame's
        if ((DateTime.Now - lastFrame).TotalMilliseconds >= animationSpeed)
        {
            lastFrame = DateTime.Now;

            activeFrame++;

            if (activeFrame >= animationLibrary[activeAnimation].Length)
            {
                activeFrame = 0;
            }

            // Assign the new frame
            rend.sprite = animationLibrary[activeAnimation][activeFrame];
        }
	}

    public void Play(string animation)
    {
        switch (animation.ToLower())
        {
            case "idle": 
                activeAnimation = 0; 
                break;
            case "run":
                activeAnimation = 1;
                break;
            case "jump": 
                activeAnimation = 2;
                break;
            case "fire":
                activeAnimation = 3;
                break;
            case "slide":
                activeAnimation = 4;
                break;
        }
    }
}
