﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterControllerInput : MonoBehaviour {

    // movement config
    public float gravity = -25f;
    public float runSpeed = 8f;
    public float groundDamping = 20f; // how fast do we change direction? higher means faster
    public float inAirDamping = 5f;
    public float jumpHeight = 3f;
    public float slideThreshold = 1100;

    private bool lastTurnGrounded = true;

    [HideInInspector]
    private float normalizedHorizontalSpeed = 0;

    private CharacterController2D _controller;
    private CharacterAnimator _animator;
    private RaycastHit2D _lastControllerColliderHit;
    private Vector3 _velocity;
    private CharacterControllerSpell _spell;

    public GameObject landEffect;

    private bool doubleJump = true; // True means you can double jump.


    void Awake()
    {
        _animator = GetComponent<CharacterAnimator>();
        _controller = GetComponent<CharacterController2D>();
        _spell = GetComponent<CharacterControllerSpell>();

        // listen to some events for illustration purposes
        _controller.onControllerCollidedEvent += onControllerCollider;
        _controller.onTriggerEnterEvent += onTriggerEnterEvent;
        _controller.onTriggerExitEvent += onTriggerExitEvent;
    }


    #region Event Listeners

    void onControllerCollider(RaycastHit2D hit)
    {
        // bail out on plain old ground hits cause they arent very interesting
        if (hit.normal.y == 1f)
            return;

        // logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
        //Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
    }


    void onTriggerEnterEvent(Collider2D col)
    {
        Debug.Log("onTriggerEnterEvent: " + col.gameObject.name);
    }


    void onTriggerExitEvent(Collider2D col)
    {
        Debug.Log("onTriggerExitEvent: " + col.gameObject.name);
    }

    #endregion
    Vector3 cachedVelocity;
    bool isSliding = false;
    DateTime hangTime = DateTime.Now;
    DateTime slideTime = DateTime.Now;
    // the Update loop contains a very simple example of moving the character around and controlling the animation
    void Update()
    {
        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;

        if (_controller.isGrounded)
        {
            double millisecondsInAir = (DateTime.Now - hangTime).TotalMilliseconds;
            // Use the hang time to determine if we should slide.
            if (millisecondsInAir >= slideThreshold)
            {
                isSliding = true;
                slideTime = DateTime.Now;
            }

            _velocity.y = 0;
            // Reset our double jump
            doubleJump = true;
            hangTime = DateTime.Now;

            if (lastTurnGrounded == false)
            {
                // Spawn a land effect
                GameObject.Instantiate(landEffect, transform.position, transform.rotation);
            }
        }

        lastTurnGrounded = _controller.isGrounded;

        if (isSliding)
        {
            // Only slide for 1 second
            if ((DateTime.Now - slideTime).TotalSeconds >= 0.5)
            {
                isSliding = false;
            }

            cachedVelocity = _velocity;
            _velocity *= .80f; // Slow the velocity down a bit.
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            normalizedHorizontalSpeed = 1;
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            if (_controller.isGrounded)
                _animator.Play("Run");
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            normalizedHorizontalSpeed = -1;
            if (transform.localScale.x > 0f)
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);

            if (_controller.isGrounded)
                _animator.Play("Run");
        }
        else
        {
            normalizedHorizontalSpeed = 0;

            if (_controller.isGrounded)
                _animator.Play("Idle");
        }


        // we can only jump whilst grounded
        if ((_controller.isGrounded || doubleJump) && (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)))
        {
            float v = Mathf.Sqrt(2f * jumpHeight * -gravity);

            if (!_controller.isGrounded)
            {
                // This was a double jump, cut back the velocity a bit
                doubleJump = false;
                v *= 0.7f;
            }

            // Jumping breaks a slide
            if (isSliding)
            {
                isSliding = false;
                _velocity = cachedVelocity;
            }

            _velocity.y = v;
            _animator.Play("Jump");
        }


        // apply horizontal speed smoothing it
        var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);

        // apply gravity before moving
        _velocity.y += gravity * Time.deltaTime;

        _controller.move(_velocity * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space)) // When it's been "pressed" call to cast our spell system.
        {
            _spell.Cast();
        }

        if (Input.GetKey(KeyCode.Space)) // If space is down at all show the animation
        {
            _animator.Play("fire");
        }

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            _animator.Play("slide");
        }

        if (isSliding)
        {
            _animator.Play("slide");
        }
    }
}
