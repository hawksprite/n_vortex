﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterControllerSpell : MonoBehaviour {

    [HideInInspector]
    public Vial activeVial;

    private GameObject deathPrefabFire;
    private GameObject deathPrefabWater;
    private GameObject deathPrefabGrass;

    private GameObject projectilePrefab;

    private DateTime lastCast;
    private Vector3 center;

    public float spellVelocity = 20.0f;
    public int castDelay = 2;

	// Use this for initialization
	void Start () {
        // Just give the player a randomized vial.
        activeVial = new Vial();
        activeVial.Randomize();

        // Find our base projectile prefab
        projectilePrefab = (GameObject)Resources.Load("Prefabs/Projectile", typeof(GameObject));

        // Find our death prefabs
        deathPrefabFire = (GameObject)Resources.Load("Prefabs/FireSpellDeath", typeof(GameObject));
        deathPrefabWater = (GameObject)Resources.Load("Prefabs/FireSpellDeath", typeof(GameObject));
        deathPrefabGrass = (GameObject)Resources.Load("Prefabs/GrassSpellDeath", typeof(GameObject));

        // Calculate center screen once to save on performance
        center = new Vector3(Screen.width / 2.0f, Screen.height / 2.0f,0.0f);

        lastCast = DateTime.Now;

        // Alert the player stats that we've spawned.
        GameObject.FindObjectOfType<PlayerStats>().Setup(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LaunchProjectile()
    {
        // Spawn a prefab for our projectile and set it up with our current vial.
        GameObject p = (GameObject)GameObject.Instantiate(projectilePrefab, transform.position, transform.rotation);
        // Now make the projectile setup call
        p.GetComponent<SpellProjectile>().Setup((Input.mousePosition - center).normalized, spellVelocity, getDeathPrefab(activeVial), activeVial);
    }

    public GameObject getDeathPrefab(Vial target)
    {
        return deathPrefabGrass;
    }


    // Entry point for recieveing the command to cast a spell.
    public void Cast()
    {
        if ((DateTime.Now - lastCast).TotalMilliseconds >= castDelay)
        {
            LaunchProjectile(); // Launch a projectile.
            lastCast = DateTime.Now;
        }
    }
}
